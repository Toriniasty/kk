package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

func entryMaker() []configDetails {
	var files, directories []string
	var cDetails []configDetails

	files, directories = scanDirectory()

	for _, v := range directories {
		cDetails = append(cDetails, configDetails{Entry: v, Directory: true})
	}

	for _, v := range files {
		var kc kubeConfig
		var tmp string

		yamlFile, err := ioutil.ReadFile(g.configDirectory + "/" + g.currDir + "/" + v)

		if err != nil {
			errHandler(err)
		}

		err = yaml.Unmarshal(yamlFile, &kc)
		if err != nil {
			errHandler(err)
		}

		for _, v := range kc.Contexts {
			if v.Name == kc.CurrentContext {
				tmp = v.Context.Cluster
			}
		}

		for _, v := range kc.Clusters {
			if v.Name == tmp {
				tmp = v.Cluster.Server
			}
		}

		cDetails = append(cDetails, configDetails{
			Entry:          v,
			Directory:      false,
			CurrentContext: kc.CurrentContext,
			Server:         tmp,
		})
	}

	return cDetails
}

func scanDirectory() ([]string, []string) {
	var files, directories []string
	var scanDir string

	if g.currDir == "" {
		scanDir = userHomeDir()
	} else {
		scanDir = userHomeDir() + g.currDir
	}

	contents, err := ioutil.ReadDir(scanDir)
	if err != nil {
		errHandler(err)
	}

	for _, c := range contents {
		if isFile(scanDir + c.Name()) {
			files = append(files, c.Name())
		} else {
			directories = append(directories, c.Name())
		}
	}

	return files, directories
}

func ifExist(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	}

	return false
}

func isFile(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		errHandler(err)
	}

	if fileInfo.IsDir() {
		return false
	}

	return true
}

func userHomeDir() string {
	return fmt.Sprintf("%v/.kube/kk/", os.Getenv("HOME"))
}

func setUpConfig(fullPath string) {
	if ifExist(g.mainKubeConfigFile) {
		err := os.Remove(g.mainKubeConfigFile)
		if err != nil {
			errHandler(err)
		}
	}

	err := os.Symlink(fullPath, g.mainKubeConfigFile)
	if err != nil {
		errHandler(err)
	}

	err = os.Chmod(g.mainKubeConfigFile, 0600)
	if err != nil {
		errHandler(err)
	}

	os.Exit(0)
}

func errHandler(err error) {
	fmt.Printf("[ERROR]: %v\n", err)
	os.Exit(1)
}

func handleArg(args []string) {

	if len(args) > 0 {
		args = args[:1]

		pathFromArg := strings.Join(args, "")
		if ifExist(pathFromArg) {
			setUpConfig(pathFromArg)
		}
		if ifExist(g.configDirectory + "/" + pathFromArg) {
			setUpConfig(g.configDirectory + "/" + pathFromArg)
		}
	}

}
