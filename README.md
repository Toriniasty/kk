# KK

Simple KubeKonfig management script.  

## Quick setup

``` bash
go install gitlab.com/Toriniasty/kk@latest
mkdir -p ~/.kube/kk
```

## Configuring

Create `~/.kube/kk` directory and put your Kubernetes configuration files over there.  
You can put them in subdirectories to give them some hierarchy.  
Once kk will recognise your choice is a file, it will symlink it as a main Kubernetes config file(`~/.kube/config`) and exit.

## Running
Assuming you have ~/go/bin in your $PATH:  
`kk`  

if not:  
`~/go/bin/kk`

## Demo
[![asciicast](https://asciinema.org/a/TOTHMHiPQAiSpRKSs5K88aG4f.png)](https://asciinema.org/a/TOTHMHiPQAiSpRKSs5K88aG4f)
