package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
)

var g struct {
	configDirectory    string
	mainKubeConfigFile string
	currDir            string
}

func init() {
	g.configDirectory = userHomeDir()
	g.mainKubeConfigFile = fmt.Sprintf("%v../config", g.configDirectory)

	if !ifExist(g.configDirectory) {
		fmt.Printf("kk main config directory doesn't exist\n")
		fmt.Printf("Please create %v and put your kubernetes config files there\n", g.configDirectory)
		fmt.Printf(`For example:
	%vTest/OneClusterConfigFile
	%vTest/AnotherClusterConfigFile
	%vProd/AnotherClusterConfigFile
	%vProd/OtterClusterConfigFile
`, g.configDirectory, g.configDirectory, g.configDirectory, g.configDirectory)
		os.Exit(1)
	}

	if ifExist(g.mainKubeConfigFile) {
		fiL, _ := os.Lstat(g.mainKubeConfigFile)
		if fiL.Mode()&os.ModeSymlink != os.ModeSymlink {
			fmt.Println("Error, main config file is not a symlink")
			os.Exit(1)
		}
	}

}

func main() {
	handleArg(os.Args[1:])

	templates := &promptui.SelectTemplates{
		Label:    `{{"\x1b[1m\x1b[34m"}}{{ . }}{{"\x1b[0m"}}`,
		Active:   `{{"\x1b[1m\x1b[34m"}}⎈ {{ .Entry }}{{if .Directory}}{{"/"}}{{end}}{{"\x1b[0m"}}`,
		Inactive: `  {{ .Entry | cyan }}{{if .Directory}}{{"/"}}{{end}}`,
		Selected: `{{"\x1b[1m\x1b[34m"}}⎈ {{ .Entry }}{{if .Directory}}{{"/"}}{{end}}{{"\x1b[0m"}}`,
		Details: `{{ if not .Directory }}{{"\x1b[1m\x1b[34m"}}
+————————Konfig Details:
|{{ "Selection:" }} {{ .Entry }}
|{{ "Context  :" }} {{ .CurrentContext }}
|{{ "Server   :" }} {{ .Server }}
+————————————————————————————————{{"\x1b[0m"}}{{end}}
`,
	}

	for {
		entries := entryMaker()
		if len(entries) == 0 {
			err := fmt.Errorf("%v/%v Directory doesnt' have any files", g.configDirectory, g.currDir)
			errHandler(err)
		}

		searcher := func(input string, index int) bool {
			entry := entries[index]
			name := strings.Replace(strings.ToLower(entry.Entry), " ", "", -1)
			input = strings.Replace(strings.ToLower(input), " ", "", -1)

			return strings.Contains(name, input)
		}

		prompt := promptui.Select{
			Label:             "Konfigs:",
			Items:             entries,
			Templates:         templates,
			Size:              10,
			Searcher:          searcher,
			StartInSearchMode: true,
		}

		i, _, err := prompt.Run()
		choice := entries[i].Entry

		if err != nil {
			errHandler(err)
		}

		fPath := fmt.Sprintf("%v%v%v", g.configDirectory, g.currDir, choice)
		if isFile(fPath) {
			setUpConfig(fPath)
		} else {
			g.currDir = g.currDir + choice + "/"
		}

	}
}
