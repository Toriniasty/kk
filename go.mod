module gitlab.com/Toriniasty/kk

go 1.13

require (
	github.com/manifoldco/promptui v0.9.0
	golang.org/x/sys v0.0.0-20220422013727-9388b58f7150 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
